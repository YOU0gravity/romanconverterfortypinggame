# coding: utf-8
import MeCab
import json
import codecs
import sys
from collections import OrderedDict

#TODO: 半角アルファベット・数字も変換できるようにしたい

REPLACABLE_KANA_DIC = {\
"ア":"a", "イ":"i", "ウ":"u", "エ":"e", "オ":"o",\
"カ":"ka", "キ":"ki", "ク":"ku", "ケ":"ke", "コ":"ko",\
"サ":"sa", "シ":"si", "ス":"su", "セ":"se", "ソ":"so",\
"タ":"ta", "チ":"ti", "ツ":"tu", "テ":"te", "ト":"to",\
"ナ":"na", "ニ":"ni", "ヌ":"nu", "ネ":"ne", "ノ":"no",\
"ハ":"ha", "ヒ":"hi", "フ":"hu", "ヘ":"he", "ホ":"ho",\
"マ":"ma", "ミ":"mi", "ム":"mu", "メ":"me", "モ":"mo",\
"ヤ":"ya", "ユ":"yu", "ヨ":"yo", \
"ラ":"ra", "リ":"ri", "ル":"ru", "レ":"re", "ロ":"ro",\
"ワ":"wa", "ヲ":"wo", "ン":"n",\
"ガ":"ga", "ギ":"gi", "グ":"gu", "ゲ":"ge", "ゴ":"go",\
"ザ":"za", "ジ":"ji", "ズ":"zu", "ゼ":"ze", "ゾ":"zo",\
"ダ":"da", "ヂ":"di", "ヅ":"du", "デ":"de", "ド":"do",\
"バ":"ba", "ビ":"bi", "ブ":"bu", "ベ":"be", "ボ":"bo",\
"パ":"pa", "ピ":"pi", "プ":"pu", "ペ":"pe", "ポ":"po",\
"ヴ":"vu",\
"ァ":"xa", "ィ":"xi", "ゥ":"xu", "ェ":"xe", "ォ":"xo",\
"ャ":"xya", "ュ":"xyu", "ョ":"xyo",\
"ッ":"xtu",\
"ー":"-", "、":",", "。":".", "？":"?", "！":"!",\
"あ":"a", "い":"i", "う":"u", "え":"e", "お":"o",\
"か":"ka", "き":"ki", "く":"ku", "け":"ke", "こ":"ko",\
"さ":"sa", "し":"si", "す":"su", "せ":"se", "そ":"so",\
"た":"ta", "ち":"ti", "つ":"tu", "て":"te", "と":"to",\
"な":"na", "に":"ni", "ぬ":"nu", "ね":"ne", "の":"no",\
"は":"ha", "ひ":"hi", "ふ":"hu", "へ":"he", "ほ":"ho",\
"ま":"ma", "み":"mi", "む":"mu", "め":"me", "も":"mo",\
"や":"ya", "ゆ":"yu", "よ":"yo", \
"ら":"ra", "り":"ri", "る":"ru", "れ":"re", "ろ":"ro",\
"わ":"wa", "を":"wo", "ん":"n",\
"が":"ga", "ぎ":"gi", "ぐ":"gu", "げ":"ge", "ご":"go",\
"ざ":"za", "じ":"zi", "ず":"zu", "ぜ":"ze", "ぞ":"zo",\
"だ":"da", "ぢ":"di", "づ":"du", "で":"de", "ど":"do",\
"ば":"ba", "び":"bi", "ぶ":"bu", "べ":"be", "ぼ":"bo",\
"ぱ":"pa", "ぴ":"pi", "ぷ":"pu", "ぺ":"pe", "ぽ":"po",\
"ぁ":"xa", "ぃ":"xi", "ぅ":"xu", "ぇ":"xe", "ぉ":"xo",\
"ゃ":"xya", "ゅ":"xyu", "ょ":"xyo",\
"っ":"xtu",\
"・":"/",\
}


REPLACABLE_STRING_TUPLES = (\
#"i"に続く"ャュョ", "ジャ, ジュ, ジョ(ixyの後に実行)"
('ixy','y'),('jy','j'),\
#"フゥ", "フゥ"以外のファ行, "ヴゥ"以外のヴァ行, "ウィ", "ウェ" ,"ウォ"
('huxu','fwu'),('hhux','ff'),('hux','f'),('vux','v'),('uxi','wi'),('uxe','we'),('uxo','wo'),\
#"ツゥ"以外のツァ行, トァ行, "ティ", "ディ", "ジェ",
('tux','ts'),('tox','tw'),('texi','thi'),('dexi','dhi'),('jixe','je'),\
#"ん"に続く"n",
('nn','nnn'),\
)

ORDERED_REPLACABLE_STRING_TUPLES = OrderedDict(REPLACABLE_STRING_TUPLES)

#これ一つを呼び出すことで、漢字混じりの日本語を、訓練式ローマ字へと変換することができる
def convert_japanese_to_roman(sentence_jp):
	#
	#	漢字交じりの日本語を、カナ文字へと変換
	#

	sentence_kana = mt.parse(sentence_jp)


	#
	#	カナ文字をローマ字へと変換
	#

	kana_length = (len(sentence_kana) - 1) / 3
	count = 0
	creating_sentence_array = []

	while(count < kana_length):
		converting_kana = sentence_kana[count*3:3+count*3]

		doesExist = False
		for one_key in REPLACABLE_KANA_DIC.keys():
			if one_key == converting_kana:
				doesExist = True
				break

		if doesExist == True:
			converted_roman = REPLACABLE_KANA_DIC[converting_kana]
			creating_sentence_array.append(converted_roman)

		count = count + 1

	sentence_rm = "".join(creating_sentence_array)

	#
	#	置き換え可能なローマ字を処理
	#

	#"ッ"を、後に続く子音に応じて、シンプルな形へ置き換え
	while sentence_rm.find('xtu') != -1:
		index = sentence_rm.find('xtu')
		if index + 3 < len(sentence_rm):
			replacing_char = sentence_rm[index + 3]
			sentence_rm = sentence_rm.replace('xtu', replacing_char, 1)
		else:	#文章の最後の文字が"ッ"である場合、置き換えはせず、ループを抜ける
			break

	#辞書のキーで文章を検索し、発見した部分を辞書の値へと置き換える
	for key in ORDERED_REPLACABLE_STRING_TUPLES.iteritems():
		result = ""

		# print "key: " +  key[0]
		# print "val: " + key[1]

		if sentence_rm.find(key[0]) != -1:
			result =  sentence_rm.replace(key[0], key[1])
			# print "The char '" + key[0] + "' has been replaced with '" + key[1] + "'."

		if result != "":
			sentence_rm = result

	if sentence_rm[-1:] == "n":	#最後の文字が"n"であるとき、"n"を一つ追加
		sentence_rm = sentence_rm + "n"

	return sentence_rm


#
#	処理開始
#

mt = MeCab.Tagger("-Oyomi")

INPUT_TEXT_REF  = 'sentenceJp.txt'
OUTPUT_TEXT_REF = 'questions.json'


print "最低文字数を半角数字で入力してください"
min_len = raw_input(">>>")
if min_len.isdigit() == False:
	min_len = 0
else:
	min_len = int(min_len)

print "最高文字数を入力してください"
max_len = raw_input(">>>")
if max_len.isdigit() == False:
	max_len = 100
else:
	max_len = int(max_len)

print str(min_len) + "文字以上" + str(max_len) + "文字以下の文章を出力します(y or n)"
answer = raw_input(">>>")
if answer != "y" and answer != "yes" and answer != "Y":
	print "処理を終了します"
	sys.exit()



print "'" + INPUT_TEXT_REF + "'の日本語文章データを読み取ります\n"

f = open(INPUT_TEXT_REF)
data = f.read()
f.close()

alphabet_array = [[] for i in range(26)]

lines = data.split('\n')

print "日本語文章データをローマ字文章データへ変換します\n"
for line in lines:
	sentence_jp = line
	sentence_rm = convert_japanese_to_roman(line)

	#文字数が指定した範囲から外れる場合、配列に保存しない
	lenOfSentence = len(sentence_rm)
	if lenOfSentence >= min_len and lenOfSentence <= max_len:
		sentence_obj = [sentence_jp, sentence_rm]

		initial_number = ord(sentence_rm[0:1]) - 97
		if initial_number >= 0 and initial_number < 26:
			alphabet_array[initial_number].append(sentence_obj)

# #テスト用
# sentence_jp = "インフォ"
# sentence_rm = convert_japanese_to_roman(sentence_jp)
# print sentence_rm

export_data = json.dumps(alphabet_array, indent=4)

# print export_data

print "ローマ字文章データを'" + OUTPUT_TEXT_REF + "'へ出力します\n"

f = codecs.open(OUTPUT_TEXT_REF, 'w', 'utf-8')
f.write(export_data)
f.close()

print "正常に処理を完了しました\n"